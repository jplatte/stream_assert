#![no_std]

#[doc(hidden)]
pub mod __private {
    use futures_util::{FutureExt, Stream, StreamExt};

    pub fn next_now_or_never<T>(s: &mut (impl Stream<Item = T> + Unpin)) -> Option<Option<T>> {
        s.next().now_or_never()
    }
}

/// Assert that the stream is ready, i.e. calling `poll_next` returns
/// `Poll::Ready(Some(_))`.
///
/// Semantically equivalent to `stream.next().now_or_never().unwrap().unwrap()`,
/// but produces nicer error messages in case the stream is not ready.
#[macro_export]
macro_rules! assert_ready {
    ($stream:expr) => {
        match $crate::__private::next_now_or_never(&mut $stream) {
            ::core::option::Option::Some(::core::option::Option::Some(val)) => val,
            ::core::option::Option::Some(::core::option::Option::None) => {
                ::core::panic!("assertion failed: stream is not ready, it's closed");
            }
            ::core::option::Option::None => {
                ::core::panic!("assertion failed: stream is not ready, it's pending");
            }
        }
    };
}

/// Assert that the stream is pending, i.e. calling `poll_next` returns
/// `Poll::Pending`.
///
/// Semantically equivalent to
/// `assert!(matches!(stream.next().now_or_never(), None))`, but produces nicer
/// error messages in case the stream is not pending.
#[macro_export]
macro_rules! assert_pending {
    ($stream:expr) => {
        match $crate::__private::next_now_or_never(&mut $stream) {
            ::core::option::Option::Some(::core::option::Option::Some(val)) => {
                ::core::panic!("assertion failed: stream is not pending, it yielded `{:?}`", val);
            }
            ::core::option::Option::Some(::core::option::Option::None) => {
                ::core::panic!("assertion failed: stream is not pending, it's closed");
            }
            ::core::option::Option::None => {}
        }
    };
}

/// Assert that the stream is closed, i.e. calling `poll_next` returns
/// `Poll::Ready(None)`.
///
/// Semantically equivalent to
/// `assert!(matches!(stream.next().now_or_never().unwrap(), None))`, but
/// produces nicer error messages in case the stream is not closed.
#[macro_export]
macro_rules! assert_closed {
    ($stream:expr) => {
        match $crate::__private::next_now_or_never(&mut $stream) {
            ::core::option::Option::Some(::core::option::Option::Some(val)) => {
                ::core::panic!("assertion failed: stream is not closed, it yielded `{:?}`", val);
            }
            ::core::option::Option::Some(::core::option::Option::None) => {}
            ::core::option::Option::None => {
                ::core::panic!("assertion failed: stream is not closed, it's pending");
            }
        }
    };
}

/// Assert that the stream is ready with a value that is equal to the given
/// expression.
#[macro_export]
macro_rules! assert_next_eq {
    ($stream:expr, $val:expr) => {
        match $crate::assert_ready!($stream) {
            val => {
                ::core::assert_eq!(val, $val);
            }
        }
    };
}

/// Assert that the stream is ready with a value that matches the given pattern.
///
/// Accepts an optional `=> expression` after the pattern that is evaluated if
/// the pattern matched. The expression can reference any bindings from the
/// pattern. The result of the expression is returned from the macro call.
#[macro_export]
macro_rules! assert_next_matches {
    ($stream:expr, $pat:pat) => {
        $crate::assert_next_matches!($stream, $pat => {})
    };
    ($stream:expr, $pat:pat => $arm:expr) => {
        match $crate::assert_ready!($stream) {
            $pat => $arm,
            val => {
                ::core::panic!(
                    "assertion failed: `{:?}` does not match `{}`",
                    val, ::core::stringify!($pat)
                );
            }
        }
    };
}
