# stream_assert

Macros to simplify testing of `Stream` based APIs.

Provides the following assertion macros:

```rust
// Assert that the next value is ready and equal to the given expression.
assert_next_eq!(stream, expression);

// Assert that the next value is ready and matches the given pattern.
assert_next_matches!(stream, Enum::Variant { field: 1, .. });

// Assert that the stream is not ready (`poll_next` returns `Pending`).
assert_pending!(stream);

// Assert that the stream is closed (`poll_next` returns `Ready(None)`).
assert_closed!(stream);
```
