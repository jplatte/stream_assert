use futures_util::stream;
use stream_assert::assert_next_matches;

#[test]
#[should_panic(expected = "assertion failed: stream is not ready, it's closed")]
fn next_closed() {
    assert_next_matches!(stream::empty(), "test");
}
